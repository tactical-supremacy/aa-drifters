# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [0.4.4a] - 2024-11026

### Fixed

- Allow Config manytomany's to be blank to unset options

## [0.4.3a] - 2024-05-11

### Updated

- Translations

## [0.4.2a] - 2023-11-15

### Fixed

- Recreated default manager after creating customs in 0.4.1

## [0.4.1] - 2023-11-10

### Fixed

- Archived and Reserved holes are now properly filtered where needed, Route has a Reserved option to include reserved holes and the scout UI will show them

## [0.4.0] - 2023-11-09

### Added

- Simple Garbage Collection Task

## [0.3.0a] - yyyy-mm-dd

### Added

- A terrible web ui, this is mostly as a proof of concept
- Spanish, Ukrainian Translations

### Changed

### Fixed

- Dependency Routing should be AA Routing

## [0.2.0a] - 2023-11-04

### Added

- We have entered the era of AA Routing, Adds many many routing features including /drifters route from_system: to_system:
- A basic web ui, not sure where this is going but its atleast an output for testing

## [0.1.2a] - 2023-10-11

Translation maintenance, allow AA 4.x

## [0.1.1a] - 2023-10-08

Maintenance Release

## [0.1.0a] - 2023-08-02

Initial Release
