# Generated by Django 4.2.7 on 2023-11-15 02:29

import django.db.models.manager
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('drifters', '0010_general_alter_wormhole_eol_changed_at_and_more'),
    ]

    operations = [
        migrations.AlterModelManagers(
            name='wormhole',
            managers=[
                ('active_public_holes', django.db.models.manager.Manager()),
            ],
        ),
    ]
