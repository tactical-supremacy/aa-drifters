��          �      \      �     �     �     �     �     �               (     0  �   9  i   �  _   *  _   �  H   �  9   3     m     u     �  	   �  a  �     �               ,     4     M     g     �     �  �   �  �   �  �   [	  �   
  �   �
  �   S     �     �               	   
                                                                                           Archived Barbican Complex Conflux K Space Bookmarked Lifetime Remaining Mass Remaining Redoubt Sentinel This wormhole has had its stability critically disrupted by the mass of numerous ships passing through and is on the verge of collapse This wormhole has had its stability reduced by ships passing through it, but not to a critical degree yet This wormhole has not yet begun its natural cycle of decay and should last at least another day This wormhole has not yet had its stability significantly disrupted by ships passing through it This wormhole is beginning to decay, and probably won't last another day This wormhole is reaching the end of its natural lifetime Vidette W Space Bookmarked Wormhole Wormholes Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-10-08 10:25+0000
Last-Translator: Joel Falknau <ozirascal@gmail.com>, 2023
Language-Team: Ukrainian (https://app.transifex.com/alliance-auth/teams/107430/uk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: uk
Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n % 10 == 1 && n % 100 != 11 ? 0 : n % 1 == 0 && n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14) ? 1 : n % 1 == 0 && (n % 10 ==0 || (n % 10 >=5 && n % 10 <=9) || (n % 100 >=11 && n % 100 <=14 )) ? 2: 3);
 Архівовано Barbican Комплекс Conflux Закладки K-Space Залишок життя Залишкова маса Redoubt Sentinel Стабільність цієї червоточини була критично порушена масою численних кораблів, які проходили через неї, і вона на межі руйнування. Ця червоточина отримала зниження стабільності від кораблів, що проходили через неї, але ще не до критичного ступеня. Ця червоточина ще не розпочала свій природний цикл розпаду і повинна існувати щонайменше ще один день. Ця червоточина ще не зазнала суттєвого порушення стабільності від кораблів, що проходили через неї. Ця червоточина починає розпадатися і, ймовірно, не протримається і дня. Ця червоточина наближається до завершення свого природного життєвого циклу. Vidette Закладки W-Space Червоточина Червоточини 